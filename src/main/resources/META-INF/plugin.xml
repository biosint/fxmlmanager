<idea-plugin>
  <id>cz.bios.idea</id>
  <name>FXMLManager</name>
  <version>2.1.2</version>
  <vendor email="ostricker@bios.cz" url="http://www.bios.cz">BIOS s.r.o.</vendor>

  <description><![CDATA[
  <h2>FXMLManager!</h2>
  <p>When clicking right mouse button on .fxml file, there is new menu item "Update Controller from FXML".</p>
  <p>Clicking this item will modify FXML Java Controller:</p>
  <ol>
    <li>Remove all @FXML fields that are missing in FXML and their getters/setters</li>
    <li>Add all @FXML fields that are missing in Controller</li>
    <li>@Deprecate all ActionEvent methods that are missing in FXML</li>
    <li>Create all ActionEvent methods that are missing from Controller</li>
  </ol>
  ]]></description>

  <change-notes>
    <![CDATA[
      <ul>
        <li><b>2.1.2</b> Fixed action thread deprecation</li>
        <li><b>2.1.1</b> Fixed XML tag call outside of Read Action</li>
        <li><b>2.1.0</b> Performance upgrade and bugfixes</li>
          <ul>
            <li>Loading FXML Nodes to map instead of list. Increase performance when using on large files</li>
            <li>FXML Node no longer need ID for Java handler to be created in class</li>
            <li>Duplicates won´t be created when multiple FXML Nodes have same handler</li>
            <li>If Java field or method already exist in class, plugin will use them and update annotations</li>
            <li>Indicator better reflects current state of the process</li>
            <li>Informational notification at the end of the process</li>
          </ul>
        </li>
        <li><b>2.0.2</b> Update to Java 17 and changes because of deprecated API</li>
        <li><b>2.0.1</b> Configuration change</li>
        <li><b>2.0.0</b> Complete refactor of all parts
          <ul>
            <li>Major restructuring of the codebase to make it maintainable in the future - complete rewrite</li>
            <li>If controller class is not found, plugin will try to create new one</li>
            <li>FXML methods without parameters (initialize()) are not longer deprecated if not found in FXML</li>
            <li>Java fields that do not correspond with FXML type get changed to right data type</li>
            <li>Standardization of TableColumns ignores columns with attribute resizable=false</li>
            <li>Modal window during plugin actions</li>
            <li>Deterministic progress indicator during processing</li>
          </ul>
        </li>
        <li><b>1.1.8</b> Reverse back to Java 11 because of Idea backwards compatibility</li>
        <li><b>1.1.7</b> Upgrade to Java 17</li>
        <li><b>1.1.6</b> Changed Controller lookup method to support different file structures</li>
        <li><b>1.1.5</b> Added nullChecks. Plugin should no longer throw NullPointerExceptions</li>
        <li><b>1.1.4</b> Updated parameters for on*action* methods</li>
        <li><b>1.1.3</b> Configuration settings update</li>
        <li><b>1.1.2</b> Bugfix - Added "Controller" string at the end of include field name</li>
        <li><b>1.1.1</b>
          <ul>
            <li>Added - Correct import of fx:include (if type is not found for some reason, it defaults to "FileTypeNotFound")</li>
            <li>Deleting fields also deletes getters and setters</li>
            <li>Small stateless info bar at right bottom corner while updating is running</li>
          </ul>
        </li>
        <li><b>1.1.0</b> Added - saving FXML and JAVA file before doing changes.</li>
        <li><b>1.0.2</b> Optimized saving Controller after changes.</li>
        <li><b>1.0.1</b> Optimized loops and changes in large files.</li>
        <li><b>1.0.0</b> Initial release. Basic logic for field and method handling.</li>
      </ul>
    ]]>
  </change-notes>

  <!-- please see https://www.jetbrains.org/intellij/sdk/docs/basics/getting_started/build_number_ranges.html for description -->
  <idea-version since-build="232"/>

  <!-- please see https://www.jetbrains.org/intellij/sdk/docs/basics/getting_started/plugin_compatibility.html
       on how to target different products -->
  <depends>com.intellij.modules.platform</depends>
  <depends>com.intellij.java</depends>

  <extensions defaultExtensionNs="com.intellij">
    <!-- Add your extensions here -->
    <notificationGroup id="FXMLManager Notification Group"
                       displayType="BALLOON"
                       key="cz.bios.plugins.fxmlmanager.notifications"/>
  </extensions>

  <actions>
    <!-- Add your actions here -->

    <action id="cz.bios.plugins.fxmlmanager.actions.UpdateControllerAction"
            class="cz.bios.plugins.fxmlmanager.actions.UpdateControllerAction"
            text="Update Controller from FXML"
            description="Updates Controller.java with changes from FXML">
      <add-to-group group-id="ProjectViewPopupMenu" relative-to-action="CutCopyPasteGroup" anchor="before"/>
    </action>
    <action id="cz.bios.plugins.fxmlmanager.actions.ColumnWidthAction"
            class="cz.bios.plugins.fxmlmanager.actions.ColumnWidthAction"
            text="Standardize FXML TableColumn Widths"
            description="Updates FXML TableColumns based on pref-width - min-width=30/max-width=(pref-width * 10)">
      <add-to-group group-id="ProjectViewPopupMenu" relative-to-action="CutCopyPasteGroup" anchor="before"/>
    </action>
  </actions>

</idea-plugin>