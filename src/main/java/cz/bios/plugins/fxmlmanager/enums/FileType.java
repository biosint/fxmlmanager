package cz.bios.plugins.fxmlmanager.enums;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * Document : fxmlmanager.main - FileType.java
 * Created on : 3.10.2022, 14:22
 * @author : BIOS s.r.o (<a href="http://bios.cz">http://bios.cz</a>) Jan Olšanský mladší
 *
 * 1.0.0: 3.10.2022: JOml - vytvoření souboru
 **/

public enum FileType
{
  FXML("fxml"),
  JAVA("java");

  public final String extension;

  FileType(String extension)
  {
    this.extension = extension;
  }

  /**
   * Parse koncovky souboru
   * @param extension
   * @return
   */
  public static FileType parseExtension(String extension)
  {
    // Pokud je prázdný tak rovnou vracím neznámý typ
    if (StringUtils.isEmpty(extension))
    {
      return null;
    }

    // Kontrola jestli existuje daný typ v enumu
    for (FileType fileType : values())
    {
      if (Objects.equals(fileType.extension.toUpperCase(), extension.toUpperCase()))
      {
        return fileType;
      }
    }

    // Nenašel jsem - vracím null
    return null;
  }

  public String getExtension()
  {
    return extension;
  }
}
