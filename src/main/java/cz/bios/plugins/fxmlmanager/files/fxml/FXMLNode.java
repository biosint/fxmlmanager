package cz.bios.plugins.fxmlmanager.files.fxml;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import cz.bios.plugins.fxmlmanager.common.NamedNodeMapIterable;
import cz.bios.plugins.fxmlmanager.common.Utils;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Node;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Document : fxmlmanager.main - FXMLNode.java
 * Created on : 4.10.2022, 10:41
 *
 * @author : BIOS s.r.o (<a href="http://bios.cz">http://bios.cz</a>) Jan Olšanský mladší
 *
 * 1.0.0: 4.10.2022: JOml - vytvoření souboru
 **/

public class FXMLNode
{
  private final Node node;
  private final List<FXMLHandler> handlerList = new ArrayList<>();

  private String dataType;
  private String id;
  private String controllerPath;

  /**
   * Konstruktor a zpracování nodu
   *
   * @param node
   */
  public FXMLNode(Node node, VirtualFile nodeVFile, Project currentProject) throws Exception
  {
    // Nastavení nodu
    this.node     = node;
    this.dataType = node.getNodeName();

    // Iterace atributy
    for (Node attribute : NamedNodeMapIterable.of(node.getAttributes()))
    {
      // Speciální akce podle jména atributu
      switch (FXMLName.parseName(attribute.getNodeName()))
      {
        case ATR_CONTROLLER:
          this.controllerPath = attribute.getNodeValue();
          break;
        case ATR_FX_ID:
          this.id = attribute.getNodeValue();
          break;
      }

      // Přidání handlerů
      if (attribute.getNodeName().startsWith("on") && !StringUtils.isEmpty(attribute.getNodeValue()))
      {
        handlerList.add(new FXMLHandler(attribute));
      }
    }

    // Pokud jde o include, tak musím nastavit dataType
    if (FXMLName.parseName(node.getNodeName()) == FXMLName.TYPE_INCLUDE)
    {
      // Získání zdroje
      Node sourceNode = node.getAttributes().getNamedItem(FXMLName.ATR_SOURCE.getName());
      if (sourceNode == null)
      {
        return;
      }

      // Získání cesty ke zdrojovému controlleru
      VirtualFile sourceVFile = Utils.retriveVirtualFileByPathFromOrigin(sourceNode.getNodeValue(), nodeVFile.getPath());

      // Parse zdrojovým FXML a načtení jeho controlleru jako datového typu
      FXMLFile sourceFXML = new FXMLFile(sourceVFile, currentProject);
      String sourceController = sourceFXML.getControllerPath();
      if (!StringUtils.isEmpty(sourceController))
      {
        // Datový typ je název controlleru
        this.dataType = new File(sourceController.replace(".", "/")).getName();

        // Název proměnné je id + Controller
        this.id = id + "Controller";
      }
    }
  }

  public Node getNode()
  {
    return node;
  }

  public List<FXMLHandler> getHandlerList()
  {
    return handlerList;
  }

  public String getId()
  {
    return id;
  }

  public String getControllerPath()
  {
    return controllerPath;
  }

  public String getDataType()
  {
    return dataType;
  }
}
