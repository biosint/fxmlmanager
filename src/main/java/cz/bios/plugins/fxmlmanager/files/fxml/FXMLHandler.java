package cz.bios.plugins.fxmlmanager.files.fxml;

import org.w3c.dom.Node;

/**
 * Document : fxmlmanager.main - FXMLHandler.java
 * Created on : 4.10.2022, 12:48
 *
 * @author : BIOS s.r.o (<a href="http://bios.cz">http://bios.cz</a>) Jan Olšanský mladší
 *
 * 1.0.0: 4.10.2022: JOml - vytvoření souboru
 **/

public class FXMLHandler
{
  private final Node node;
  private final String name;
  private final String parameterType;
  private final String parameterName;

  public FXMLHandler(Node node)
  {
    this.node = node;
    this.name = node.getNodeValue().replace("#", "");

    if (FXMLName.parseName(node.getNodeName()) == FXMLName.ATR_ON_ACTION)
    {
      this.parameterType = "ActionEvent";
      this.parameterName = "actionEvent";
    }
    else
    {
      this.parameterType = "Event";
      this.parameterName = "event";
    }
  }

  public Node getNode()
  {
    return node;
  }

  public String getName()
  {
    return name;
  }

  public String getParameterType()
  {
    return parameterType;
  }

  public String getParameterName()
  {
    return parameterName;
  }
}
