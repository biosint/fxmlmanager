package cz.bios.plugins.fxmlmanager.files.fxml;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * Document : fxmlmanager.main - FXMLName.java
 * Created on : 4.10.2022, 10:35
 *
 * @author : BIOS s.r.o (<a href="http://bios.cz">http://bios.cz</a>) Jan Olšanský mladší
 *
 * 1.0.0: 4.10.2022: JOml - vytvoření souboru
 **/

public enum FXMLName
{
  NEZNAME(""),

  TYPE_TABLECOLUMN("TableColumn"),
  TYPE_INCLUDE("fx:include"),

  ATR_PREF_WIDTH("prefWidth"),
  ATR_MAX_WIDTH("maxWidth"),
  ATR_MIN_WIDTH("minWidth"),
  ATR_RESIZABLE("resizable"),
  ATR_SOURCE("source"),
  ATR_ON_ACTION("onAction"),
  ATR_CONTROLLER("fx:controller"),
  ATR_FX_ID("fx:id");

  public final String name;

  FXMLName(String name)
  {
    this.name = name;
  }

  /**
   * Parse koncovky souboru
   * @param name
   * @return
   */
  public static FXMLName parseName(String name)
  {
    // Pokud je prázdný tak rovnou vracím neznámý typ
    if (StringUtils.isEmpty(name))
    {
      return NEZNAME;
    }

    // Kontrola jestli existuje daný typ v enumu
    for (FXMLName fxmlName : values())
    {
      if (Objects.equals(fxmlName.name.toUpperCase(), name.toUpperCase()))
      {
        return fxmlName;
      }
    }

    // Nenašel jsem - vracím null
    return NEZNAME;
  }

  public String getName()
  {
    return name;
  }
}
