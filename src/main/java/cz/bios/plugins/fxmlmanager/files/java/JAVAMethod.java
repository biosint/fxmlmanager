package cz.bios.plugins.fxmlmanager.files.java;

import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiParameter;
import cz.bios.plugins.fxmlmanager.common.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Document : fxmlmanager.main - JAVAMethod.java
 * Created on : 4.10.2022, 13:52
 *
 * @author : BIOS s.r.o (<a href="http://bios.cz">http://bios.cz</a>) Jan Olšanský mladší
 *
 * 1.0.0: 4.10.2022: JOml - vytvoření souboru
 **/

public class JAVAMethod
{
  private final PsiMethod psiMethod;
  private String name;
  private final List<JAVAAnnotation> annotationList = new ArrayList<>();
  private final List<JAVAParams> paramsList = new ArrayList<>();

  public JAVAMethod(PsiMethod psiMethod)
  {
    this.psiMethod = psiMethod;

    Utils.runReadAction(() ->
    {
      this.name = psiMethod.getName();

      // Naplnění známých anotací
      for (PsiAnnotation annotation : psiMethod.getModifierList().getAnnotations())
      {
        // Zjištění jestli jde o známou anotaci
        JAVAAnnotation javaAnnotation = JAVAAnnotation.parseAnnotation(annotation.getQualifiedName());
        if (javaAnnotation != null)
        {
          annotationList.add(javaAnnotation);
        }
      }

      // Informace o parametrech
      for (PsiParameter parameter : psiMethod.getParameterList().getParameters())
      {
        paramsList.add(new JAVAParams(parameter.getType().getCanonicalText(), parameter.getName()));
      }
    });
  }

  /**
   * Přidej anotaci
   */
  public void addAnnotation(JAVAAnnotation annotation)
  {
    Utils.runWriteAction(psiMethod.getProject(), () -> psiMethod.getModifierList().addAnnotation(annotation.getAnnotation()));
  }

  /**
   * Smazání požadované anotace
   * @param annotation
   */
  public synchronized void deleteAnnotation(JAVAAnnotation annotation)
  {
    // Zjištění jestli field má danou anotaci - musí proběhnout v Read Akci
    AtomicReference<PsiAnnotation> annToDelete = new AtomicReference<>();
    Utils.runReadAction(() -> annToDelete.set(psiMethod.getModifierList().findAnnotation(annotation.getAnnotation())));

    if (annToDelete.get() != null)
    {
      // Pokud ano tak mažu -> ve write akci
      Utils.runWriteAction(psiMethod.getProject(), () -> annToDelete.get().delete());
    }
  }

  public PsiMethod getPsiMethod()
  {
    return psiMethod;
  }

  public String getName()
  {
    return name;
  }

  public List<JAVAAnnotation> getAnnotationList()
  {
    return annotationList;
  }

  public List<JAVAParams> getParamsList()
  {
    return paramsList;
  }
}
