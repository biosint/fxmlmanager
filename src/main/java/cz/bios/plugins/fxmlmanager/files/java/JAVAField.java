package cz.bios.plugins.fxmlmanager.files.java;

import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.search.searches.ReferencesSearch;
import cz.bios.plugins.fxmlmanager.common.FXMLManagerException;
import cz.bios.plugins.fxmlmanager.common.Utils;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Document : fxmlmanager.main - JAVAField.java
 * Created on : 4.10.2022, 13:52
 *
 * @author : BIOS s.r.o (<a href="http://bios.cz">http://bios.cz</a>) Jan Olšanský mladší
 *
 * 1.0.0: 4.10.2022: JOml - vytvoření souboru
 **/

public class JAVAField
{
  private final PsiField psiField;
  private String dataType;
  private String name;
  private final List<JAVAAnnotation> annotationList = new ArrayList<>();

  public JAVAField(PsiField psiField)
  {
    this.psiField = psiField;

    Utils.runReadAction(() ->
    {
      this.name     = psiField.getName();
      this.dataType = psiField.getType().getCanonicalText();

      if (psiField.getModifierList() != null)
      {
        // Naplnění známých anotací
        for (PsiAnnotation annotation : psiField.getModifierList().getAnnotations())
        {
          // Zjištění jestli jde o známou anotaci
          JAVAAnnotation javaAnnotation = JAVAAnnotation.parseAnnotation(annotation.getQualifiedName());
          if (javaAnnotation != null)
          {
            annotationList.add(javaAnnotation);
          }
        }
      }
    });
  }

  /**
   * Smazání pole ze souboru
   */
  public void delete()
  {
    Utils.runWriteAction(psiField.getProject(), () ->
    {
      ReferencesSearch.search(psiField).forEach(psiReference ->
      {
        PsiMethod refMethod = null;

        // Reference je v metodě dost vnořená, takže hledám vysoko
        PsiElement potentGetMethod = psiReference.getElement().getParent().getParent().getParent();
        PsiElement potentSetMethod = psiReference.getElement().getParent().getParent().getParent().getParent();

        // Testuji jestli mám metody
        if (potentGetMethod instanceof PsiMethod)
        {
          refMethod = (PsiMethod) potentGetMethod;
        }
        else if (potentSetMethod instanceof PsiMethod)
        {
          refMethod = (PsiMethod) potentSetMethod;
        }

        // Pokud jsem získal metody, tak testuji jestli to jsou generované getter nebo setter metody
        if (refMethod != null)
        {
          String capitalizedField = StringUtils.capitalize((psiField).getName());
          if (refMethod.getName().equals("set" + capitalizedField) || refMethod.getName().equals("get" + capitalizedField))
          {
            // Odmazání metody
            refMethod.delete();
          }
        }
      });

      // Odmazání fieldu
      psiField.delete();
    });
  }

  /**
   * Přidání anotace k poli
   * @param javaAnnotation
   */
  public void addAnnotation(JAVAAnnotation javaAnnotation)
  {
    Utils.runWriteAction(psiField.getProject(), () ->
    {
      if (psiField.getModifierList() == null)
      {
        throw new FXMLManagerException("Internal error during PsiField modification");
      }

      // Přidání anotace
      psiField.getModifierList().addAnnotation(javaAnnotation.getAnnotation());
    });
  }

  /**
   * Smazání anotace k poli
   * @param javaAnnotation
   */
  public void deleteAnnotation(JAVAAnnotation javaAnnotation)
  {
    // Zjištění jestli field má danou anotaci - musí proběhnout v Read Akci
    AtomicReference<PsiAnnotation> annToDelete = new AtomicReference<>();
    Utils.runReadAction(() ->
    {
      if (psiField.getModifierList() == null)
      {
        throw new FXMLManagerException("Internal error during PsiField modification");
      }

      annToDelete.set(psiField.getModifierList().findAnnotation(javaAnnotation.getAnnotation()));
    });

    if (annToDelete.get() != null)
    {
      // Pokud ano tak mažu -> ve write akci
      Utils.runWriteAction(psiField.getProject(), () -> annToDelete.get().delete());
    }
  }

  public PsiField getPsiField()
  {
    return psiField;
  }

  public String getDataType()
  {
    return dataType;
  }

  public String getName()
  {
    return name;
  }

  public List<JAVAAnnotation> getAnnotationList()
  {
    return annotationList;
  }
}
