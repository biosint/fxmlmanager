package cz.bios.plugins.fxmlmanager.files.fxml;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.xml.XmlFile;
import com.intellij.psi.xml.XmlTag;
import cz.bios.plugins.fxmlmanager.common.CommonFile;
import cz.bios.plugins.fxmlmanager.common.FXMLManagerException;
import cz.bios.plugins.fxmlmanager.common.Utils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Document : fxmlmanager.main - FXMLFile.java
 * Created on : 3.10.2022, 16:42
 *
 * @author : BIOS s.r.o (<a href="http://bios.cz">http://bios.cz</a>) Jan Olšanský mladší
 *
 * 1.0.0: 3.10.2022: JOml - vytvoření souboru
 **/

public class FXMLFile extends CommonFile
{
  private final List<FXMLNode> nodesList = new ArrayList<>();
  private XmlFile xmlFile;

  /**
   * Konstruktor
   * @param virtualFile
   */
  public FXMLFile(VirtualFile virtualFile, Project project) throws Exception
  {
    super(virtualFile, project);

    // Načtení FXML do XmlFile
    Utils.runReadAction(() -> this.xmlFile = (XmlFile) PsiManager.getInstance(getCurrentProject()).findFile(getVirtualFile()));

    // Kontrola, že proběhlo v pořádku
    if (xmlFile == null)
    {
      throw new FXMLManagerException("FXMLFile.constructor: Could not load FXMLFile to XmlFile object");
    }

    // Zpracování souboru - načtení nodů do listu
    processFile();

    // Uložení souboru před prací
    Utils.commitFile(getCurrentProject(), (PsiFile) xmlFile);
  }

  /**
   * Parsování souboru - načtení informací do objektů
   */
  public void processFile() throws Exception
  {
    // Na začátku vyčistím list nodů
    nodesList.clear();

    // Nastavení objektů pro parse XML
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();

    // Parse dokumentem
    try (FileInputStream fxmlInputStream = FileUtils.openInputStream(getFile()))
    {
      InputSource inputSource = new InputSource(fxmlInputStream);
      Document document = builder.parse(inputSource);

      // Rekursivní zpracování nodů v dokumentu
      processNode(document.getDocumentElement());
    }
  }

  /**
   * Rekursivní funkce na zpracování nodů
   * @param node
   */
  private void processNode(Node node) throws Exception
  {
    // Přidání nodu do listu
    nodesList.add(new FXMLNode(node, getVirtualFile(), getCurrentProject()));

    // Zpracuj children nodes
    NodeList nodeList = node.getChildNodes();
    for (int i = 0; i < nodeList.getLength(); i++)
    {
      Node childNode = nodeList.item(i);
      if (childNode.getNodeType() == Node.ELEMENT_NODE)
      {
        // Rekursivně zpracuj child node
        processNode(childNode);
      }
    }
  }

  /**
   * Vrátí všechny XMLTagy z FXML -> jejich editace změní soubor
   * @return
   */
  public List<XmlTag> getXMLTagList()
  {
    return getXMLTagList(xmlFile.getRootTag());
  }

  /**
   * Rekursivní metoda pro načtení XmlTag a podřízených
   * @return
   */
  public List<XmlTag> getXMLTagList(XmlTag xmlTag)
  {
    List<XmlTag> listXmlTags = new ArrayList<>();
    listXmlTags.add(xmlTag);

    // Čtení subTags musí proběhnout v ReadAkci
    Utils.runReadAction(() ->
    {
      for (XmlTag subTag : xmlTag.getSubTags())
      {
        listXmlTags.addAll(getXMLTagList(subTag));
      }
    });

    return listXmlTags;
  }

  /**
   * Vrací controller path z listu nodů nebo null
   * @return
   */
  public String getControllerPath()
  {
    Optional<FXMLNode> controllerNode = nodesList.stream().filter(node -> !StringUtils.isEmpty(node.getControllerPath())).findFirst();
    return controllerNode.map(FXMLNode::getControllerPath).orElse(null);
  }

  public List<FXMLNode> getNodesList()
  {
    return nodesList;
  }

  public XmlFile getXmlFile()
  {
    return xmlFile;
  }
}
