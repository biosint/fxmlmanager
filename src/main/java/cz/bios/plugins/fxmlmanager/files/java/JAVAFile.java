package cz.bios.plugins.fxmlmanager.files.java;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.util.PsiUtil;
import cz.bios.plugins.fxmlmanager.common.CommonFile;
import cz.bios.plugins.fxmlmanager.common.FXMLManagerException;
import cz.bios.plugins.fxmlmanager.common.Utils;
import org.apache.commons.io.FilenameUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * Document : fxmlmanager.main - JAVAFile.java
 * Created on : 3.10.2022, 16:42
 *
 * @author : BIOS s.r.o (<a href="http://bios.cz">http://bios.cz</a>) Jan Olšanský mladší
 *
 * 1.0.0: 3.10.2022: JOml - vytvoření souboru
 **/

public class JAVAFile extends CommonFile
{
  private final List<JAVAField> fieldsList = new ArrayList<>();
  private final List<JAVAMethod> methodsList = new ArrayList<>();

  private PsiElement[] psiElements;
  private PsiJavaFile psiJavaFile;
  private PsiClass psiMainClass;

  /**
   * Konstruktor
   * @param virtualFile
   */
  public JAVAFile(VirtualFile virtualFile, Project project) throws Exception
  {
    super(virtualFile, project);

    // Zpracování souboru - načtení fieldů a method do listu
    processFile();
  }

  /**
   * Parsování souboru - načtení informací do objektů
   */
  public void processFile()
  {
    fieldsList.clear();
    methodsList.clear();

    Utils.runReadAction(() ->
    {
      // Získání Program Structure Interface a editace Controller.java
      this.psiJavaFile = (PsiJavaFile) PsiManager.getInstance(getCurrentProject()).findFile(getVirtualFile());
      if (psiJavaFile == null)
      {
        throw new FXMLManagerException("JAVAFile.constructor: PsiJavaFile not found");
      }

      // Parse třídami java souboru a získání hlavní třídy (ignoruji anonymní)
      for (PsiClass clazz : psiJavaFile.getClasses())
      {
        if (Objects.equals(clazz.getName(), FilenameUtils.getBaseName(getVirtualFile().getPath())))
        {
          this.psiMainClass = clazz;
          break;
        }
      }

      // Class check
      if (psiMainClass == null)
      {
        throw new FXMLManagerException("JAVAFile.constructor: PsiClass not found inside PsiJavaFile");
      }

      // Naplnění všech elementů do listu
      this.psiElements = psiMainClass.getChildren();
    });

    // Rozlišení elemtů a naplnění listu
    for (PsiElement psiElement : psiElements)
    {
      if (psiElement instanceof PsiField)
      {
        fieldsList.add(new JAVAField((PsiField) psiElement));
      }
      else if (psiElement instanceof PsiMethod)
      {
        methodsList.add(new JAVAMethod((PsiMethod) psiElement));
      }
    }

    // Uložení souboru před prací
    Utils.commitFile(getCurrentProject(), psiJavaFile);
  }

  /**
   * Vytvoření fieldu třídy
   */
  public void createField(String dataType, String name, String psiModifier, List<JAVAAnnotation> annotationList)
  {
    Utils.runWriteAction(getCurrentProject(), () ->
    {
      // Získání element factory
      PsiElementFactory factory = JavaPsiFacade.getInstance(getCurrentProject()).getElementFactory();

      // Vytvoření typu a nového fieldu
      PsiType type = factory.createTypeFromText(dataType, null);
      PsiField newField = factory.createField(name, type);
      if (newField.getModifierList() == null)
      {
        throw new FXMLManagerException("Internal error during PsiField creation");
      }

      // Doplnění požadovaných anotací
      for (JAVAAnnotation annotation : annotationList)
      {
        newField.getModifierList().addAnnotation(annotation.getAnnotation());
      }

      // Nastavení modifikátoru - public/private
      PsiUtil.setModifierProperty(newField, psiModifier, true);

      // Přidání do třídy
      psiMainClass.add(newField);
    });
  }

  /**
   * Vytvoření metody
   */
  public void createMethod(PsiType returnType, String name, String psiModifier, List<JAVAParams> parameterList, List<JAVAAnnotation> annotationList)
  {
    Utils.runWriteAction(getCurrentProject(), () ->
    {
      // Získání element factory
      PsiElementFactory factory = JavaPsiFacade.getInstance(getCurrentProject()).getElementFactory();

      // Vytvoření typu a nové metody
      PsiMethod newMethod = factory.createMethod(name, returnType);

      // Doplnění požadovaných anotací
      for (JAVAAnnotation annotation : annotationList)
      {
        newMethod.getModifierList().addAnnotation(annotation.getAnnotation());
      }

      // Nastavení modifikátoru - public/private
      PsiUtil.setModifierProperty(newMethod, psiModifier, true);

      // Naplnění parametrů
      for (JAVAParams param : parameterList)
      {
        // Vytvoření parametru a doplnění do metody
        PsiParameter psiParam = factory.createParameter(param.name(), factory.createTypeFromText(param.dataType(), null));
        newMethod.getParameterList().add(psiParam);
      }

      // Přidání do třídy
      psiMainClass.add(newMethod);
    });
  }

  /**
   * Vrací všechny fieldy s požadovanou anotací
   * @param annotation
   * @return
   */
  public List<JAVAField> getFieldsListWithAnnotation(JAVAAnnotation annotation)
  {
    return fieldsList.stream().filter(javaField -> javaField.getAnnotationList().contains(annotation)).collect(Collectors.toList());
  }

  /**
   * Vrací všechny metody s požadovanou anotací
   * @param annotation
   * @return
   */
  public List<JAVAMethod> getMethodsListWithAnnotation(JAVAAnnotation annotation)
  {
    return methodsList.stream().filter(javaMethod -> javaMethod.getAnnotationList().contains(annotation)).collect(Collectors.toList());
  }

  public List<JAVAField> getFieldsList()
  {
    return fieldsList;
  }

  public List<JAVAMethod> getMethodsList()
  {
    return methodsList;
  }
}
