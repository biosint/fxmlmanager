package cz.bios.plugins.fxmlmanager.files.java;

import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

/**
 * Document : fxmlmanager.main - JAVAAnnotation.java
 * Created on : 5.10.2022, 10:18
 *
 * @author : BIOS s.r.o (<a href="http://bios.cz">http://bios.cz</a>) Jan Olšanský mladší
 *
 * 1.0.0: 5.10.2022: JOml - vytvoření souboru
 **/

public enum JAVAAnnotation
{
  FXML("javafx.fxml.FXML"),
  DEPRECATED("java.lang.Deprecated");

  public final String annotation;

  JAVAAnnotation(String annotation)
  {
    this.annotation = annotation;
  }

  /**
   * Parse anotace
   * @param annotation
   * @return
   */
  public static JAVAAnnotation parseAnnotation(String annotation)
  {
    // Pokud je prázdný tak rovnou vracím neznámý typ
    if (StringUtils.isEmpty(annotation))
    {
      return null;
    }

    // Kontrola jestli existuje daný typ v enumu
    for (JAVAAnnotation javaAnnotation : values())
    {
      if (Objects.equals(javaAnnotation.annotation.toUpperCase(), annotation.toUpperCase()))
      {
        return javaAnnotation;
      }
    }

    // Nenašel jsem - vracím null
    return null;
  }

  public String getAnnotation()
  {
    return annotation;
  }
}
