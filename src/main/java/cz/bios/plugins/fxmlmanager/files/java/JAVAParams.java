package cz.bios.plugins.fxmlmanager.files.java;

/**
 * Document : fxmlmanager.main - JAVAParams.java
 * Created on : 5.10.2022, 11:30
 * @author : BIOS s.r.o (<a href="http://bios.cz">http://bios.cz</a>) Jan Olšanský mladší
 *     <p>
 *     1.0.0: 5.10.2022: JOml - vytvoření souboru
 **/

public record JAVAParams(String dataType, String name)
{
}
