package cz.bios.plugins.fxmlmanager.actions.columnWidth;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.psi.xml.XmlTag;
import cz.bios.plugins.fxmlmanager.common.CommonActionProcessor;
import cz.bios.plugins.fxmlmanager.common.FXMLManagerException;
import cz.bios.plugins.fxmlmanager.common.Utils;
import cz.bios.plugins.fxmlmanager.files.fxml.FXMLFile;
import cz.bios.plugins.fxmlmanager.files.fxml.FXMLName;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Document : fxmlmanager.main - ColumnWidthProcessor.java
 * Created on : 3.10.2022, 14:22
 * @author : BIOS s.r.o (<a href="http://bios.cz">http://bios.cz</a>) Jan Olšanský mladší
 *
 * 1.0.0: 3.10.2022: JOml - vytvoření souboru
 **/

public class ColumnWidthProcessor extends CommonActionProcessor
{

  /**
   * Konstruktor
   *
   * @param anActionEvent
   */
  public ColumnWidthProcessor(AnActionEvent anActionEvent)
  {
    super(anActionEvent);
  }

  @Override
  public void run()
  {
    try
    {
      // Zpracování FXML souboru
      FXMLFile fxmlFile = new FXMLFile(getActionEvent().getData(PlatformDataKeys.VIRTUAL_FILE), getCurrentProject());

      // Načtení sloupců
      List<XmlTag> listSloupcu = fxmlFile
        .getXMLTagList()
        .stream()
        .filter(xmlTag -> Objects.equals(xmlTag.getName(), FXMLName.TYPE_TABLECOLUMN.getName()))
        .toList();

      // Nastavení fraction pro progres
      double fraction = 1 / (double) listSloupcu.size();

      // Loop nody a nastavení sloupců
      for (XmlTag xmlTag : listSloupcu)
      {
        // Načtení preferenční a maximální šířky
        AtomicReference<String> prefWidth = new AtomicReference<>();
        AtomicReference<String> maxWidth = new AtomicReference<>();
        AtomicReference<String> resizible = new AtomicReference<>();
        Utils.runReadAction(() ->
        {
          prefWidth.set(xmlTag.getAttributeValue(FXMLName.ATR_PREF_WIDTH.getName()));
          maxWidth.set(xmlTag.getAttributeValue(FXMLName.ATR_MAX_WIDTH.getName()));
          resizible.set(xmlTag.getAttributeValue(FXMLName.ATR_RESIZABLE.getName()));
        });

        // Pokud existuje tag - resizible
        if (!StringUtils.isEmpty(resizible.get()))
        {
          // Pokud je resizible nastaveno na false, tak přeskakuji
          if (!Boolean.parseBoolean(resizible.get()))
          {
            continue;
          }
        }

        double width;
        if (!StringUtils.isEmpty(prefWidth.get()))
        {
          width = Double.parseDouble(prefWidth.get());
        }
        else if (!StringUtils.isEmpty(maxWidth.get()))
        {
          width = Double.parseDouble(maxWidth.get()) / 10;
        }
        else
        {
          throw new FXMLManagerException("Could not determine prefered width of TableColumn (" + xmlTag.getAttributeValue(FXMLName.ATR_FX_ID.getName()) + ")");
        }

        // Check jestli můžu nastavovat šířky správně
        if (width < 1)
        {
          throw new FXMLManagerException("Could not determine prefered width of TableColumn (" + xmlTag.getAttributeValue(FXMLName.ATR_FX_ID.getName()) + ")");
        }

        // Nastavení atributů
        Utils.runWriteAction(getCurrentProject(), () ->
        {
          xmlTag.setAttribute(FXMLName.ATR_PREF_WIDTH.getName(), Double.toString(width));
          xmlTag.setAttribute(FXMLName.ATR_MAX_WIDTH.getName(), String.valueOf(width * 10));
          xmlTag.setAttribute(FXMLName.ATR_MIN_WIDTH.getName(), "30.0");
        });

        // Update indikátoru
        getIndicator().setFraction(getIndicator().getFraction() + fraction);
      }

      // Uložení
      fxmlFile.save();
    }
    catch (Exception e)
    {
      setThrowable(e);
    }
  }
}
