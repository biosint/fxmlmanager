package cz.bios.plugins.fxmlmanager.actions.updateController;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiModifier;
import com.intellij.psi.PsiTypes;
import cz.bios.plugins.fxmlmanager.common.CommonActionProcessor;
import cz.bios.plugins.fxmlmanager.common.FXMLManagerException;
import cz.bios.plugins.fxmlmanager.common.Utils;
import cz.bios.plugins.fxmlmanager.files.fxml.FXMLFile;
import cz.bios.plugins.fxmlmanager.files.fxml.FXMLHandler;
import cz.bios.plugins.fxmlmanager.files.fxml.FXMLNode;
import cz.bios.plugins.fxmlmanager.files.java.*;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Document : fxmlmanager - UpdateControllerProcessor.java
 * Created on : 3.10.2022, 14:22
 * @author : BIOS s.r.o (<a href="http://bios.cz">http://bios.cz</a>) Jan Olšanský mladší
 *
 * 1.0.0: 3.10.2022: JOml - vytvoření souboru
 **/

public class UpdateControllerProcessor extends CommonActionProcessor
{
  long deletedFields = 0L;
  long deprecatedMethods = 0L;
  long createdFields = 0L;
  long updatedFields = 0L;
  long createdMethods = 0L;
  long updatedMethods = 0L;

  /**
   * Konstruktor
   *
   * @param anActionEvent
   */
  public UpdateControllerProcessor(AnActionEvent anActionEvent)
  {
    super(anActionEvent);
  }

  @Override
  public void run()
  {
    try
    {
      getIndicator().setFraction(0);

      // Zpracování FXML souboru
      FXMLFile fxmlFile = new FXMLFile(getActionEvent().getData(PlatformDataKeys.VIRTUAL_FILE), getCurrentProject());

      // Získání virutálního souboru Java třídy
      VirtualFile javaVirtualFile = Utils.retriveVirtualFileByPackagePath(fxmlFile.getControllerPath(), "java", getCurrentProject());
      if (javaVirtualFile == null)
      {
        // V případě že nenajdu soubor, tak vytvářím novou třídu na požadovaném místě
        javaVirtualFile = Utils.createJavaFile(fxmlFile.getControllerPath(), getCurrentProject());
        if (javaVirtualFile == null)
        {
          throw new FXMLManagerException("Controller class not found and could not be created. (" + fxmlFile.getControllerPath() + ")");
        }
      }

      // Zpracování Java třídy
      JAVAFile javaFile = new JAVAFile(javaVirtualFile, getCurrentProject());

      //region -------------------------------- PŘÍPRAVA LISTŮ PRO ODMAZÁNÍ --------------------------------
      // Mapa identifikátorů nodů z FXML -> pouze ty které mají ID
      Map<String, FXMLNode> fxmlNodeMap = fxmlFile.getNodesList().stream()
          .filter(node -> !StringUtils.isEmpty(node.getId()))
          .collect(Collectors.toMap(FXMLNode::getId, node -> node));

      // Získání mapy všech handlerů - název handleru, objekt
      Map<String, FXMLHandler> fxmlHandlerMap = fxmlFile.getNodesList().stream()
          .flatMap(node -> node.getHandlerList().stream()).toList().stream()
          .collect(Collectors.toMap(FXMLHandler::getName, handler -> handler, (existingValue, newValue) -> existingValue));
      //endregion

      //region -------------------------------- ODMAZÁNÍ FIELDŮ --------------------------------
      // Spočítání fieldů k odmazání - zajímají mě anotované FXML fieldy, které nejsou mezi identifikátory ve FXML
      List<JAVAField> javaFieldList = javaFile.getFieldsListWithAnnotation(JAVAAnnotation.FXML);

      // Kalkulace počítadla
      int totalIterations = javaFieldList.size();
      double increment = 0.25 / totalIterations;

      // Loop fieldy
      for (JAVAField javaField : javaFieldList)
      {
        // Najdi korespondující node
        FXMLNode node = fxmlNodeMap.get(javaField.getName());
        if (node == null || !javaField.getDataType().contains(node.getDataType()))
        {
          // Pokud jsem node nenašel nebo neodpovídá datový typ, tak odmazávám field
          javaField.delete();
          deletedFields++;
        }

        // Update počítadla
        getIndicator().setFraction(getIndicator().getFraction() + increment);
      }

      getIndicator().setFraction(0.25);
      //endregion

      //region -------------------------------- DEPRECATE METOD --------------------------------
      List<JAVAMethod> javaMethodList = javaFile.getMethodsListWithAnnotation(JAVAAnnotation.FXML);

      // Kalkulace počítadla
      totalIterations = javaMethodList.size();
      increment = 0.25 / totalIterations;

      // Procházení všemi metodami, které mají anotaci FXML
      for (JAVAMethod javaMethod : javaMethodList)
      {
        // Najdi korespondující node
        FXMLHandler handler = fxmlHandlerMap.get(javaMethod.getName());

        // Pokud má metoda existující parametry -> tedy funguje jako event handler, tak ji nastavuji depricated
        // Pokud nemá parametry, tak to může být inicializační metoda a té nenastavuji nic
        if (handler == null && !javaMethod.getParamsList().isEmpty())
        {
          // Nenašel jsem korespondující handler -> nastavuji deprecated
          javaMethod.deleteAnnotation(JAVAAnnotation.FXML);
          javaMethod.addAnnotation(JAVAAnnotation.DEPRECATED);
          deprecatedMethods++;
        }

        // Update počítadla
        getIndicator().setFraction(getIndicator().getFraction() + increment);
      }

      getIndicator().setFraction(0.5);
      //endregion

      //region -------------------------------- PŘÍPRAVA LISTŮ PRO PŘIDÁNÍ --------------------------------
      // Uložení a znovu načtení Java souboru, protože se mezitím změnily počty fieldů a stav metod
      javaFile.save();
      javaFile.processFile();

      // Mapa javaFieldů ve Controlleru pro porovnání
      Map<String, JAVAField> javaFields = javaFile.getFieldsList().stream()
          .collect(Collectors.toMap(JAVAField::getName, node -> node));

      // Mapa javaMethod ve Controlleru pro porovnání
      Map<String, JAVAMethod> javaMethods = javaFile.getMethodsList().stream()
          .collect(Collectors.toMap(JAVAMethod::getName, node -> node, (existingValue, newValue) -> existingValue));
      //endregion

      //region -------------------------------- VYTVOŘENÍ FIELDŮ --------------------------------

      // Kalkulace počítadla
      totalIterations = fxmlNodeMap.size();
      increment = 0.25 / totalIterations;

      // Procházím všechny nody, které mají ID
      for (FXMLNode node : fxmlNodeMap.values())
      {
        // Pokud FXMLNode nemá svůj counterpart v Controlleru tak ho vytvářím
        JAVAField field = javaFields.get(node.getId());
        if (field == null)
        {
          javaFile.createField(node.getDataType(), node.getId(), PsiModifier.PRIVATE, List.of(JAVAAnnotation.FXML));
          createdFields++;
        }
        else
        {
          boolean changed = false;
          // Pokud field existuje, ale mezi anotacemi není FXML, tak ho musím přidat
          if (!field.getAnnotationList().contains(JAVAAnnotation.FXML))
          {
            field.addAnnotation(JAVAAnnotation.FXML);
            changed = true;
          }

          // Obsahuje anotaci Deprecated -> odmazávám
          if (field.getAnnotationList().contains(JAVAAnnotation.DEPRECATED))
          {
            field.deleteAnnotation(JAVAAnnotation.DEPRECATED);
            changed = true;
          }

          if (changed)
          {
            updatedFields++;
          }
        }

        // Update počítadla
        getIndicator().setFraction(getIndicator().getFraction() + increment);
      }

      getIndicator().setFraction(0.75);
      //endregion

      //region -------------------------------- VYTVOŘENÍ METOD --------------------------------

      // Kalkulace počítadla
      totalIterations = fxmlHandlerMap.size();
      increment = 0.25 / totalIterations;

      // Procházím všechny nody, které mají ID
      for (FXMLHandler handler : fxmlHandlerMap.values())
      {
        // Pokud FXMLNode nemá svůj counterpart v Controlleru tak ho vytvářím
        JAVAMethod method = javaMethods.get(handler.getName());
        if (method == null)
        {
          // Naplnění parametrů
          List<JAVAParams> parameterList = new ArrayList<>();
          parameterList.add(new JAVAParams(handler.getParameterType(), handler.getParameterName()));

          // Vytvoření metody
          javaFile.createMethod(PsiTypes.voidType(), handler.getName(), PsiModifier.PUBLIC, parameterList, List.of(JAVAAnnotation.FXML));
          createdMethods++;
        }
        else
        {
          boolean changed = false;
          // Pokud field existuje, ale mezi anotacemi není FXML, tak ho musím přidat
          if (!method.getAnnotationList().contains(JAVAAnnotation.FXML))
          {
            method.addAnnotation(JAVAAnnotation.FXML);
            changed = true;
          }

          // Obsahuje anotaci Deprecated -> odmazávám
          if (method.getAnnotationList().contains(JAVAAnnotation.DEPRECATED))
          {
            method.deleteAnnotation(JAVAAnnotation.DEPRECATED);
            changed = true;
          }

          if (changed)
          {
            updatedMethods++;
          }
        }

        // Update počítadla
        getIndicator().setFraction(getIndicator().getFraction() + increment);
      }

      getIndicator().setFraction(1);
      //endregion

      javaFile.save();
    }
    catch (Exception e)
    {
      setThrowable(e);
    }
  }

  public long getDeletedFields()
  {
    return deletedFields;
  }

  public long getDeprecatedMethods()
  {
    return deprecatedMethods;
  }

  public long getCreatedFields()
  {
    return createdFields;
  }

  public long getUpdatedFields()
  {
    return updatedFields;
  }

  public long getCreatedMethods()
  {
    return createdMethods;
  }

  public long getUpdatedMethods()
  {
    return updatedMethods;
  }
}