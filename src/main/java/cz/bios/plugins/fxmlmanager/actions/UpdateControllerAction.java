package cz.bios.plugins.fxmlmanager.actions;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.ui.Messages;
import cz.bios.plugins.fxmlmanager.actions.updateController.UpdateControllerProcessor;
import cz.bios.plugins.fxmlmanager.common.CommonAction;
import cz.bios.plugins.fxmlmanager.common.FXMLManagerException;
import cz.bios.plugins.fxmlmanager.enums.FileType;
import cz.bios.plugins.fxmlmanager.notifications.FXMLManagerNotifier;
import org.jetbrains.annotations.NotNull;

/**
 * Document : FXMLManager - UpdateController.java
 * Created on : 5.8.2020, 9:5
 * @author : Jan Olšanský mladší
 *
 * 1.0.0: 5.8.2020: JOml - vytvoření souboru
 **/

public class UpdateControllerAction extends CommonAction
{
  /**
   * Update se volá při každé akci uživatele v IDEA. Měla by to být rychlá akce. Pro nás rozhoduje o zobrazení
   * @param event
   */
  @Override
  public void update(@NotNull AnActionEvent event)
  {
    // Check jestli je vybráno FXML
    boolean jeVybranoFXML = isSelectedSingleFile(event, FileType.FXML);

    // Podle toho se aktivuje
    event.getPresentation().setEnabled(jeVybranoFXML);
  }

  /**
   * Parsování souboru a aktualizace Controlleru
   * @param anActionEvent
   */
  @Override
  public void actionPerformed(@NotNull AnActionEvent anActionEvent)
  {
    // Vytvoření processoru
    UpdateControllerProcessor processor = new UpdateControllerProcessor(anActionEvent);

    // Spuštění processoru v background threadu s modálním oknem
    ProgressManager.getInstance().run(new Task.Modal(anActionEvent.getProject(), "Updating Java Controller from FXML", false)
    {
      public void run(@NotNull ProgressIndicator indicator)
      {
        // Nastavení indikátoru
        if (!indicator.isRunning())
        {
          indicator.start();
          indicator.setFraction(0);
        }

        // Spuštění procesu
        processor.setIndicator(indicator);
        processor.run();

        // Zastavení indikátoru po dojetí processoru
        indicator.stop();
      }
    });

    // Pokud jsem skončil s chybou, tak jí zobrazuji
    if (processor.getThrowable() != null)
    {
      // Informace pro uživatele
      Messages.showMessageDialog(anActionEvent.getProject(), processor.getThrowable().getMessage(), "Error", Messages.getErrorIcon());

      // Vyhoď exception aby se zapsala do logu
      throw new FXMLManagerException(processor.getThrowable());
    }

    // Počty změn
    long changeNum = processor.getDeletedFields() +
                     processor.getDeprecatedMethods() +
                     processor.getCreatedFields() +
                     processor.getUpdatedFields() +
                     processor.getCreatedMethods() +
                     processor.getUpdatedMethods();
    if (changeNum == 0L)
    {
      FXMLManagerNotifier.notifyInfo(processor.getCurrentProject(), "FXMLManager", "No changes found");
      return;
    }

    String log = "";
    if (processor.getDeletedFields() > 0L) log += " Deleted fields: %s.".formatted(processor.getDeletedFields());
    if (processor.getDeprecatedMethods() > 0L) log += " Deprecated methods: %s.".formatted(processor.getDeprecatedMethods());
    if (processor.getCreatedFields() > 0L) log += " Created fields: %s.".formatted(processor.getCreatedFields());
    if (processor.getUpdatedFields() > 0L) log += " Updated fields: %s.".formatted(processor.getUpdatedFields());
    if (processor.getCreatedMethods() > 0L) log += " Created methods: %s.".formatted(processor.getCreatedMethods());
    if (processor.getUpdatedMethods() > 0L) log += " Updated methods: %s.".formatted(processor.getUpdatedMethods());

    FXMLManagerNotifier.notifyInfo(processor.getCurrentProject(), "FXMLManager", "Controller updated successfully -" + log);
  }

  @Override
  public boolean isDumbAware()
  {
    return false;
  }
}