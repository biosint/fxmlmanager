package cz.bios.plugins.fxmlmanager.common;

import com.intellij.openapi.actionSystem.ActionUpdateThread;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.vfs.VirtualFile;
import cz.bios.plugins.fxmlmanager.enums.FileType;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/**
 * Document : fxmlmanager.main - CommonAction.java
 * Created on : 3.10.2022, 14:22
 * @author : BIOS s.r.o (<a href="http://bios.cz">http://bios.cz</a>) Jan Olšanský mladší
 *
 * 1.0.0: 3.10.2022: JOml - vytvoření souboru
 **/

public abstract class CommonAction extends AnAction
{
  @Override
  public @NotNull ActionUpdateThread getActionUpdateThread()
  {
    return ActionUpdateThread.BGT;
  }

  /**
   * Kontrola jestli je vybraný jeden soubor se správnou koncovkou
   * @param event
   * @param expectedFileType
   * @return
   */
  protected boolean isSelectedSingleFile(AnActionEvent event, FileType expectedFileType)
  {
    try
    {
      // List vybraných souborů
      VirtualFile[] fileList = event.getData(PlatformDataKeys.VIRTUAL_FILE_ARRAY);

      // Pokud není vybrán jeden soubor, tak se akce nezobrazuje
      if (fileList == null || fileList.length != 1)
      {
        return false;
      }

      // Zisk VirtualFile
      VirtualFile file = event.getData(PlatformDataKeys.VIRTUAL_FILE);

      // Zobrazení pouze pokud je soubor se správnou koncovkou
      return file != null && Objects.equals(expectedFileType, FileType.parseExtension(file.getExtension()));
    }
    catch (Exception exception)
    {
      return false;
    }
  }
}
