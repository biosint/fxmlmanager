package cz.bios.plugins.fxmlmanager.common;

/**
 * Jediná výjimka šířená modulem, použije se pro vlastní výjimečné stavy
 * i tehdy, kdy dojde k jakékoli chybě zpracování.
 * Pokud dojde např. k chybě  {@link java.io.IOException}, je šířena tato
 * výjimka a {@link java.io.IOException} je řádně označena jako její příčina
 *
 * Document : fxmlmanager.main - FXMLManagerException.java
 * Created on : 4.10.2022, 11:44
 * @author : BIOS s.r.o (<a href="http://bios.cz">http://bios.cz</a>) Jan Olšanský mladší
 *
 * 1.0.0: 4.10.2022: JOml - vytvoření souboru
 **/

public class FXMLManagerException extends RuntimeException
{
  private static final long serialVersionUID = 38149624L;

  public FXMLManagerException(String message)
  {
    super(message);
  }

  public FXMLManagerException(Throwable cause)
  {
    super(cause);
  }

  public FXMLManagerException(String message, Throwable cause)
  {
    super(message, cause);
  }
}
