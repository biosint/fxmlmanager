package cz.bios.plugins.fxmlmanager.common;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.project.Project;
import org.jetbrains.annotations.NotNull;

/**
 * Document : fxmlmanager.main - CommonProcessor.java
 * Created on : 3.10.2022, 14:22
 * @author : BIOS s.r.o (<a href="http://bios.cz">http://bios.cz</a>) Jan Olšanský mladší
 *
 * 1.0.0: 3.10.2022: JOml - vytvoření souboru
 **/

public abstract class CommonActionProcessor
{
  private final AnActionEvent anActionEvent;
  private final Project currentProject;
  private Throwable throwable = null;
  private ProgressIndicator indicator = null;

  /**
   * Konstruktor
   *
   * @param anActionEvent
   */
  public CommonActionProcessor(@NotNull AnActionEvent anActionEvent)
  {
    this.anActionEvent = anActionEvent;
    this.currentProject = anActionEvent.getProject();
  }

  /**
   * Metoda, která obsahuje vlastní processor
   */
  public abstract void run();

  public AnActionEvent getActionEvent()
  {
    return anActionEvent;
  }

  public Project getCurrentProject()
  {
    return currentProject;
  }

  public Throwable getThrowable()
  {
    return throwable;
  }

  public void setThrowable(Throwable throwable)
  {
    this.throwable = throwable;
  }

  public ProgressIndicator getIndicator()
  {
    return indicator;
  }

  public void setIndicator(ProgressIndicator indicator)
  {
    this.indicator = indicator;
  }
}
