package cz.bios.plugins.fxmlmanager.common;

import com.intellij.ide.highlighter.JavaFileType;
import com.intellij.openapi.application.ReadAction;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ProjectRootManager;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.vfs.VirtualFileManager;
import com.intellij.psi.*;
import com.intellij.psi.impl.PsiDocumentManagerImpl;
import com.intellij.util.ThrowableRunnable;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.concurrent.atomic.AtomicReference;

/**
 * Document : fxmlmanager.main - Utils.java
 * Created on : 3.10.2022, 17:12
 *
 * @author : BIOS s.r.o (<a href="http://bios.cz">http://bios.cz</a>) Jan Olšanský mladší
 *
 * 1.0.0: 3.10.2022: JOml - vytvoření souboru
 **/

public abstract class Utils
{
  /**
   * Uložení souboru
   * @param project
   * @param file
   */
  public static void commitFile(Project project, PsiFile file)
  {
    // Uložení všech neuložených dokumentů, aby se nad nimi dalo pracovat
    PsiDocumentManager documentManager = PsiDocumentManagerImpl.getInstance(project);
    Document document = documentManager.getDocument(file);
    if (document != null && !documentManager.isCommitted(document))
    {
      WriteCommandAction.runWriteCommandAction(project, () -> documentManager.commitDocument(document));
    }
  }

  /**
   * Spuštění zapisovací akce
   * @param runnable
   */
  public static void runWriteAction(Project project, @NotNull Runnable runnable)
  {
    WriteCommandAction.runWriteCommandAction(project, runnable);
  }

  /**
   * Spuštění čtecí akce
   * @param action
   */
  public static <E extends Throwable> void runReadAction(@NotNull ThrowableRunnable<E> action) throws E
  {
    ReadAction.run(action);
  }

  /**
   * Získání virutálního souboru podle origin souboru a jména cílového souboru
   * @param filePath
   * @param originFilePath
   * @return
   */
  public static VirtualFile retriveVirtualFileByPathFromOrigin(String filePath, String originFilePath)
  {
    Path basePath = FileSystems.getDefault().getPath(originFilePath);
    Path resolvedPath = basePath.getParent().resolve(filePath).normalize();
    return VirtualFileManager.getInstance().findFileByNioPath(resolvedPath);
  }

  /**
   * Získání virtuálního souboru podle jeho kompletní cesty
   * @param packageFilePath
   * @param project
   * @return
   */
  public static VirtualFile retriveVirtualFileByPackagePath(String packageFilePath, String extension, Project project)
  {
    // Získání všech souborů
    VirtualFile[] vFiles = ProjectRootManager.getInstance(project).getContentSourceRoots();
    for (VirtualFile vFile : vFiles)
    {
      File file = new File(vFile.getPath() + File.separator + packageFilePath.replace(".", File.separator) + "." + extension);
      if (file.exists())
      {
        return LocalFileSystem.getInstance().findFileByIoFile(file);
      }
    }

    return null;
  }

  /**
   * Vytvoření Java Souboru z reference
   * @param reference
   */
  public static VirtualFile createJavaFile(String reference, Project project)
  {
    // Parse názvu package name
    String className = reference.substring(reference.lastIndexOf(".")).replaceAll("\\.", "");
    String directory = reference.substring(0, reference.lastIndexOf(".")).replace(".", File.separator);
    File targetDirectory = null;

    // Prolezení projektu a pokus o nalezení directory
    VirtualFile[] vFiles = ProjectRootManager.getInstance(project).getContentSourceRoots();
    for (VirtualFile vFile : vFiles)
    {
      targetDirectory = new File(vFile.getPath() + File.separator + directory);
      if (targetDirectory.exists())
      {
        break;
      }
    }

    // Nepodařilo se najít directory
    if (targetDirectory == null)
    {
      return null;
    }

    // Vytvoření Java Třídy
    return createJavaFile(className, targetDirectory, project);
  }

  /**
   * Vytvoření Java Souboru
   * @param className
   * @param directory
   * @param project
   * @return
   */
  public static VirtualFile createJavaFile(String className, File directory, Project project)
  {
    // Načtení do virtual file
    VirtualFile parentVirtualDir = LocalFileSystem.getInstance().findFileByIoFile(directory);
    if (parentVirtualDir == null || !parentVirtualDir.isDirectory())
    {
      return null;
    }

    // Naplnění do psiDirectory
    AtomicReference<PsiDirectory> psiDirectory = new AtomicReference<>();
    runReadAction(() -> psiDirectory.set(PsiManager.getInstance(project).findDirectory(parentVirtualDir)));
    if (psiDirectory.get() == null)
    {
      return null;
    }

    // Vytvoření souboru
    AtomicReference<PsiFile> psiFile = new AtomicReference<>();
    runWriteAction(project, () ->
    {
      // Vytvoření Java třídy v paměti
      PsiFile newPsiFile = PsiFileFactory.getInstance(project).createFileFromText(className + ".java", JavaFileType.INSTANCE,
        "public class " + className
        + "\n{\n"
        + "    @javafx.fxml.FXML\n"
        + "    public void initialize() {\n"
        + "    }"
        + "}");

      // Uložení do vybraného directory -> Provede uložení na disk
      psiDirectory.get().add(newPsiFile);

      // Ukládám vše - zoufalství
      psiFile.set(psiDirectory.get().findFile(className + ".java"));
    });

    // Kontrola jestli je vše ok
    if (psiFile.get() == null)
    {
      return null;
    }

    return psiFile.get().getVirtualFile();
  }
}