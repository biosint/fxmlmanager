package cz.bios.plugins.fxmlmanager.common;

import com.intellij.openapi.editor.Document;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;

import java.io.File;

/**
 * Document : fxmlmanager.main - CommonFile.java
 * Created on : 3.10.2022, 16:43
 *
 * @author : BIOS s.r.o (<a href="http://bios.cz">http://bios.cz</a>) Jan Olšanský mladší
 *
 * 1.0.0: 3.10.2022: JOml - vytvoření souboru
 **/

public abstract class CommonFile
{
  private final VirtualFile virtualFile;
  private final File file;
  private Document document;

  private final Project currentProject;

  /**
   * Konstruktor - načtení a uložení souboru pro další práci
   * @param virtualFile
   */
  protected CommonFile(VirtualFile virtualFile, Project project)
  {
    // Získání projektu
    this.currentProject = project;

    // Nastavení virutálního souboru
    if (virtualFile == null)
    {
      throw new FXMLManagerException("CommonFile.constructor: Empty virtual file parameter");
    }

    // Vytvoření dokumentu ze souboru
    Utils.runReadAction(() -> this.document = FileDocumentManager.getInstance().getDocument(virtualFile));
    if (this.document == null)
    {
      throw new FXMLManagerException("CommonFile.constructor: Could not find document inside file object");
    }

    // Uložení souboru
    Utils.runWriteAction(currentProject, () -> FileDocumentManager.getInstance().saveDocument(document));

    // Nastavení virutálního souboru
    this.virtualFile = FileDocumentManager.getInstance().getFile(document);
    if (this.virtualFile == null)
    {
      throw new FXMLManagerException("CommonFile.constructor: Could not process file");
    }

    // Nastavení do souboru
    this.file = new File(getVirtualFile().getPath());
    if (!this.file.exists())
    {
      throw new FXMLManagerException("CommonFile.constructor: Empty File object");
    }
  }

  /**
   * Ukládací metoda
   */
  public void save()
  {
    // Uložení souboru
    Utils.runWriteAction(currentProject, () -> FileDocumentManager.getInstance().saveDocument(document));
  }

  public VirtualFile getVirtualFile()
  {
    return virtualFile;
  }

  public Document getDocument()
  {
    return document;
  }

  public Project getCurrentProject()
  {
    return currentProject;
  }

  public File getFile()
  {
    return file;
  }
}
