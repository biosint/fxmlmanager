package cz.bios.plugins.fxmlmanager.common;

import org.jetbrains.annotations.NotNull;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import java.util.Iterator;

/**
 * Document : fxmlmanager.main - NamedNodeMapIterable.java
 * Created on : 4.10.2022, 10:51
 *
 * @author : BIOS s.r.o (<a href="http://bios.cz">http://bios.cz</a>) Jan Olšanský mladší
 * Ukradeno z <a href="https://stackoverflow.com/questions/4171380/generic-foreach-iteration-of-namednodemap">...</a>
 *
 * 1.0.0: 4.10.2022: JOml - vytvoření souboru
 **/

public final class NamedNodeMapIterable implements Iterable<Node>
{
  private final NamedNodeMap namedNodeMap;

  private NamedNodeMapIterable(NamedNodeMap namedNodeMap)
  {
    this.namedNodeMap = namedNodeMap;
  }

  public static NamedNodeMapIterable of(NamedNodeMap namedNodeMap)
  {
    return new NamedNodeMapIterable(namedNodeMap);
  }

  private class NamedNodeMapIterator implements Iterator<Node>
  {
    private int nextIndex = 0;

    @Override
    public boolean hasNext()
    {
      return (namedNodeMap.getLength() > nextIndex);
    }

    @Override
    public Node next()
    {
      Node item = namedNodeMap.item(nextIndex);
      nextIndex = nextIndex + 1;
      return item;
    }

    @Override
    public void remove()
    {
      throw new UnsupportedOperationException();
    }
  }

  @NotNull
  @Override
  public Iterator<Node> iterator()
  {
    return new NamedNodeMapIterator();
  }
}