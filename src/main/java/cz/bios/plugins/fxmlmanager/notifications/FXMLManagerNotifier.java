package cz.bios.plugins.fxmlmanager.notifications;

import com.intellij.notification.NotificationGroupManager;
import com.intellij.notification.NotificationType;
import com.intellij.openapi.project.Project;

public class FXMLManagerNotifier
{
  /**
   * Zobrazí chybovou notifikaci uživateli.
   * @param project Projekt, ve kterém se má notifikace zobrazit.
   * @param content Obsah notifikace.
   */
  public static void notifyError(Project project, String content)
  {
    // Získání instance NotificationGroupManager a vytvoření chybové notifikace
    NotificationGroupManager.getInstance()
        .getNotificationGroup("FXMLManager Notification Group") // Skupina notifikací pro tento plugin
        .createNotification(content, NotificationType.ERROR) // Vytvoření notifikace s předaným obsahem a typem ERROR
        .notify(project); // Zobrazení notifikace v projektu
  }

  /**
   * Zobrazí informační notifikaci uživateli.
   * @param project Projekt, ve kterém se má notifikace zobrazit.
   * @param title   Titulek notifikace.
   * @param content Obsah notifikace.
   */
  public static void notifyInfo(Project project, String title, String content)
  {
    // Získání instance NotificationGroupManager a vytvoření informační notifikace
    NotificationGroupManager.getInstance()
        .getNotificationGroup("FXMLManager Notification Group") // Skupina notifikací pro tento plugin
        .createNotification(title, content, NotificationType.INFORMATION) // Vytvoření notifikace s předaným titulkem, obsahem a typem INFORMATION
        .notify(project); // Zobrazení notifikace v projektu
  }
}